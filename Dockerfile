FROM node:14-alpine as builder
WORKDIR /usr/src/app
COPY package*.json ./
RUN yarn install --force
COPY . .
RUN rm -rf build
RUN yarn build

FROM nginx:1.13-alpine
COPY --from=builder /usr/src/app/build /usr/share/nginx/html
COPY site.conf /etc/nginx/conf.d/default.conf
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
