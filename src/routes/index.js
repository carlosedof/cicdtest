import React from "react";
import { AnimatePresence } from "framer-motion";
import { BrowserRouter as Router, Redirect, Switch } from "react-router-dom";
import Route from "./Route";
import CommonLayout from "../features/internal/Layout";
import RoutesListPages from "./RoutesListPages";
import { ROUTES_LIST } from "./ROUTES_LIST";

export default () => (
  <Router>
    <CommonLayout>
      <AnimatePresence exitBeforeEnter>
        <Switch>
          {Object.keys(ROUTES_LIST).map((r) => (
            <Route
              key={r}
              path={ROUTES_LIST[r].path}
              component={RoutesListPages[ROUTES_LIST[r].component]}
              isPrivate={ROUTES_LIST[r].isPrivate}
              exact={ROUTES_LIST[r].exact}
            />
          ))}
          <Route path="*" component={() => <Redirect to="/404" />} isPrivate />
        </Switch>
      </AnimatePresence>
    </CommonLayout>
  </Router>
);
