import React from "react";
import { Result } from "antd";

export default () => (
  <Result
    status="404"
    title="404"
    subTitle="Desculpe, a página que você está procurando não existe."
  />
);
