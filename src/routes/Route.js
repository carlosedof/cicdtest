import React from "react";
import { Redirect, Route as ReactDOMRoute } from "react-router-dom";
import { motion } from "framer-motion";
import { useAuthentication } from "../context/AuthContext";

const Route = ({ isPrivate, roles, component: Component, ...rest }) => {
  const authenticated = useAuthentication();

  const renderMethod = ({ location }) => {
    const redirect = (to) => (
      <Redirect
        to={{
          pathname: to,
          state: { from: location },
        }}
      />
    );
    if (isPrivate && !authenticated) {
      return redirect("/login");
    }
    return (
      <motion.div
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        exit={{ opacity: 0 }}
      >
        <Component />
      </motion.div>
    );
  };

  return <ReactDOMRoute {...rest} render={(props) => renderMethod(props)} />;
};

export default Route;
