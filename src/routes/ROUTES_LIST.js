export const ROUTES_LIST = {
  LOGIN: { path: "/login", component: "Login" },
  PROJECT_PRESENTATION: {
    path: "/project-presentation/:id",
    pathWithoutId: "/project-presentation",
    component: "ProjectPresentation",
  },
  PROJECT: {
    path: "/project",
    component: "Project",
  },
  PROJECT_CREATE: {
    path: "/project-create",
    component: "ProjectCreate",
  },
  PROJECT_EDIT: {
    path: "/project-edit/:id",
    pathWithoutId: "/project-edit",
    component: "ProjectEdit",
  },
  LANGUAGE_CREATE: {
    path: "/language-create",
    component: "LanguageCreate",
  },
  LANGUAGE_EDIT: {
    path: "/language-edit/:id",
    pathWithoutId: "/language-edit",
    component: "LanguageEdit",
  },
  LANGUAGE: {
    path: "/language",
    component: "Language",
  },
  ACCOUNT: { path: "/account", component: "Account", isPrivate: true },
  REGISTRATION: { path: "/registration", component: "Registration" },
  CREATE_ACCOUNT: { path: "/account", component: "Account", isPrivate: true },
  FORGOT_PASSWORD: { path: "/forgot-password", component: "ForgotPassword" },
  CHANGE_PASSWORD: {
    path: "/change-password",
    component: "ChangePassword",
    isPrivate: true,
  },
  LOGOUT: { path: "/logout", component: "Logout", isPrivate: true },
  ROUTE404: { path: "/404", component: "Route404" },
  ROUTE403: { path: "/403", component: "Route403" },
  HOME: { path: "/", component: "Home", exact: true },
};
