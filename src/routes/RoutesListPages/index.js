import Login from "../../features/open/Login";
import Logout from "../../features/internal/Logout";
import Account from "../../features/internal/Account";
import Route404 from "../Route404";
import Route403 from "../Route403";
import Home from "../../features/open/Home";
import Registration from "../../features/open/Registration";
import ProjectPresentation from "../../features/open/ProjectPresentation";
import Project from "../../features/internal/Backoffice/Project";
import ChangePassword from "../../features/internal/ChangePassword";
import ForgotPassword from "../../features/open/ForgotPassword";
import Language from "../../features/internal/Backoffice/Language";
import LanguageCreate from "../../features/internal/Backoffice/Language/Create";
import LanguageEdit from "../../features/internal/Backoffice/Language/Edit";
import ProjectEdit from "../../features/internal/Backoffice/Project/Edit";
import ProjectCreate from "../../features/internal/Backoffice/Project/Create";

export default {
  Home,
  Login,
  Logout,
  ProjectPresentation,
  Project,
  ProjectCreate,
  ProjectEdit,
  Language,
  LanguageCreate,
  LanguageEdit,
  Account,
  Route404,
  Route403,
  ChangePassword,
  ForgotPassword,
  Registration,
};
