import React from "react";
import { Result } from "antd";

export default () => (
  <Result
    status="403"
    title="403"
    subTitle="Desculpe, você não está autorizado a acessar esta página."
  />
);
