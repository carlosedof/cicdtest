import api from "./api";

const createLanguageRequest = (_, data) => api.post("/languages", data);
const findLanguagesRequest = () => api.get("/languages");
const findLanguageRequest = (id) => api.get(`/languages/${id}`);
const updateLanguageRequest = (id, data) => api.put(`/languages/${id}`, data);
const deleteLanguageRequest = (id, data) =>
  api.delete(`/languages/${id || data}`);

export {
  createLanguageRequest,
  findLanguagesRequest,
  findLanguageRequest,
  updateLanguageRequest,
  deleteLanguageRequest,
};
