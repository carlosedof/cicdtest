import axios from "axios";
import { Storage } from "../utils";

const api = axios.create({
  baseURL: "/api",
  timeout: 20000,
});

const requestHandler = async (request) => {
  const token = await Storage.getToken();
  request.headers["Access-Control-Allow-Origin"] = "*";
  if (token != null) {
    request.headers.Authorization = `Bearer ${token}`;
  }
  return request;
};
api.interceptors.request.use((request) => requestHandler(request));

const errorResponseHandler = (error) => {
  throw error?.response?.data || error;
};

api.interceptors.response.use((response) => response, errorResponseHandler);

export default api;
