import api from "./api";
import { Toast } from "../utils";

const loginRequest = (credentials) =>
  api
    .post("/authenticate", credentials)
    .then(({ data }) => data)
    .catch((e) => Toast.showSuccessMessage(e));

const recoverPasswordRequest = (_, email) =>
  api.post("/recuperarSenhaViaEmail", email);

const changePasswordRequest = (_, newPassword) =>
  api.post("/confirmarAlteracaoSenha", newPassword);

export { loginRequest, recoverPasswordRequest, changePasswordRequest };
