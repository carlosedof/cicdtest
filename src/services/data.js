import api from "./api";

const createProjectRequest = (_, data) => api.post("/projects", data);
const findProjectsRequest = () => api.get("/projects");
const findProjectRequest = (id) => api.get(`/projects/${id}`);
const deleteProjectRequest = (id, data) =>
  api.delete(`/projects/${id || data}`);
const updateProjectRequest = (id, data) => api.put(`/projects/${id}`, data);

const uploadProjectImageRequest = (id, { file, extension }) => {
  const formData = new FormData();
  formData.append("file", file);
  const config = {
    headers: {
      "content-type": "multipart/form-data",
    },
  };
  return api.post(
    `api/modules/upload/module/${id}/extension/${extension}`,
    formData,
    config
  );
};

export {
  updateProjectRequest,
  deleteProjectRequest,
  createProjectRequest,
  findProjectsRequest,
  findProjectRequest,
  uploadProjectImageRequest,
};
