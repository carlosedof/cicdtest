import { useEffect, useState } from "react";
import { Storage } from "../../utils";
import { loginRequest } from "../../services/auth";

export default function useAuth() {
  const [authenticated, setAuthenticated] = useState(false);
  const [user, setUser] = useState(null);
  const [isFetching, setIsFetching] = useState(true);

  useEffect(() => {
    async function retrieveToken() {
      const token = await Storage.getToken();
      const userFromToken = await Storage.getUser();
      if (token) {
        setAuthenticated(true);
        setUser(userFromToken);
      }
      setIsFetching(false);
    }
    retrieveToken();
  }, []);

  async function handleLogin({ email, password }) {
    setIsFetching(true);
    try {
      const { user: userResponse, token } = await loginRequest({
        email,
        password,
      });
      await Storage.setUser(userResponse);
      await Storage.setToken(token);
      setAuthenticated(true);
      setUser(userResponse);
      setIsFetching(false);
    } catch (e) {
      setIsFetching(false);
    }
  }

  async function handleAuthenticateAfterRegistration({
    token,
    user: userFromAuth,
  }) {
    setIsFetching(true);
    await Storage.setUser(userFromAuth);
    await Storage.setToken(token);
    setAuthenticated(true);
    setUser(userFromAuth);
    setIsFetching(false);
  }

  async function handleLogout() {
    setAuthenticated(false);
    setUser(null);
    await Storage.clearToken();
    await Storage.clearUser();
  }
  return {
    authenticated,
    isFetching,
    user,
    handleLogin,
    handleLogout,
    handleAuthenticateAfterRegistration,
  };
}
