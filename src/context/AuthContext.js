import React, { createContext, useContext } from "react";

import useAuth from "./hooks/useAuth";

const Context = createContext();

function AuthProvider({ children }) {
  const {
    authenticated,
    isFetching,
    handleLogin,
    handleLogout,
    user,
    handleAuthenticateAfterRegistration,
  } = useAuth();

  return (
    <Context.Provider
      value={{
        isFetching,
        authenticated,
        handleLogin,
        handleLogout,
        user,
        handleAuthenticateAfterRegistration,
      }}
    >
      {children}
    </Context.Provider>
  );
}

export const useAuthentication = () => {
  const {
    authenticated,
    isFetching,
    user,
    handleLogout,
    handleAuthenticateAfterRegistration,
  } = useContext(Context);
  return {
    authenticated,
    user,
    isFetching,
    handleLogout,
    handleAuthenticateAfterRegistration,
  };
};

export { Context as AuthContext, AuthProvider };
