import React from "react";
import { ConfigProvider } from "antd";
import { toast, ToastContainer } from "react-toastify";
import ptBR from "antd/es/locale/pt_BR";
import "moment/locale/pt-br";
import "./antd-override.css";
import "react-multi-carousel/lib/styles.css";
import "react-slideshow-image/dist/styles.css";

import { AuthProvider } from "./context/AuthContext";

const ptbr = {
  ...ptBR,
  DatePicker: {
    ...ptBR.DatePicker,
    dateFormat: "DD/MM/YYYY",
    dateTimeFormat: "DD/MM/YYYY HH:mm:ss",
    weekFormat: "YYYY-wo",
    monthFormat: "YYYY-MM",
  },
};

const Providers = ({ children }) => {
  toast.configure();
  return (
    <AuthProvider>
      <ConfigProvider locale={ptbr}>
        <ToastContainer
          position="top-right"
          autoClose={5000}
          hideProgressBar
          newestOnTop={false}
          closeOnClick
          rtl={false}
          pauseOnFocusLoss={false}
          draggable={false}
          pauseOnHover
        />
        {children}
      </ConfigProvider>
    </AuthProvider>
  );
};

export default Providers;
