import { toast } from "react-toastify";
import React from "react";

let invalidTokenOpen = false;
let requestErrorOpen = false;

const showInfoMessage = (message) => {
  toast.info(<span>{message}</span>);
};

const showSuccessMessage = (message) => {
  toast.success(<span>{message}</span>);
};

const showWarnMessage = (message) => {
  toast.warn(<span>{message}</span>);
};

const showErrorMessage = (message) => {
  toast.error(<span>{message}</span>);
};

const showRequestErrorMessage = (message) => {
  if (!requestErrorOpen) {
    requestErrorOpen = true;
    toast.error(<span>{message}</span>, {
      onClose: () => {
        requestErrorOpen = false;
      },
    });
  }
};

const showInvalidTokenMessage = () => {
  if (!invalidTokenOpen) {
    invalidTokenOpen = true;
    toast.error(<span>Sessão expirada, efetue login novamente.</span>, {
      onClose: () => {
        invalidTokenOpen = false;
      },
    });
  }
};

export default {
  showInfoMessage,
  showSuccessMessage,
  showWarnMessage,
  showErrorMessage,
  showInvalidTokenMessage,
  showRequestErrorMessage,
};
