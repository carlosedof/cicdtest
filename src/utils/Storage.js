const store = async (key, value, json = true) => {
  try {
    await localStorage.setItem(key, json ? JSON.stringify(value) : value);
  } catch (e) {
    // saving error
  }
};

const get = async (key, json = true) => {
  try {
    const value = await localStorage.getItem(key);
    if (value !== null) {
      return json ? JSON.parse(value) : value;
    }
    return value;
  } catch (e) {
    // error reading value
    return null;
  }
};

const setFirstLoad = async () => {
  await store("firstLoad", { success: true });
};

const getFirstLoad = async () => {
  const firstLoad = await get("firstLoad");
  return firstLoad;
};

const setUser = async (token) => {
  await store("user", token);
};

const getUser = async () => {
  const token = await get("user");
  return token;
};

const setToken = async (token) => {
  await store("token", token);
};

const getToken = async () => {
  const token = await get("token");
  return token;
};

const setCart = async (cart) => {
  await store("cart", cart);
};

const getCart = async () => {
  const cart = await get("cart");
  return cart;
};

const clearToken = async () => {
  await store("token", null);
};

const clearUser = async () => {
  await store("user", null);
};

const clearCart = async () => {
  await store("cart", []);
};

export default {
  store,
  get,
  getToken,
  getUser,
  clearToken,
  clearUser,
  setToken,
  setUser,
  setFirstLoad,
  setCart,
  getFirstLoad,
  getCart,
  clearCart,
};
