import React from "react";
import AmexIcon from "../assets/images/flags/amex.png";
import DinersIcon from "../assets/images/flags/diners.png";
import DiscoverIcon from "../assets/images/flags/discover.png";
import EloIcon from "../assets/images/flags/elo.png";
import HipercardIcon from "../assets/images/flags/hipercard.png";
import JcbIcon from "../assets/images/flags/jcb.png";
import MastercardIcon from "../assets/images/flags/master.png";
import VisaIcon from "../assets/images/flags/visa.png";

function getCardFlag(cardnumber) {
  if (!cardnumber || cardnumber.length === 0) {
    return false;
  }
  const cardnumberR = cardnumber.replace(/[^0-9]+/g, "");

  const cards = {
    visa: /^4[0-9]{12}(?:[0-9]{3})/,
    mastercard: /^5[1-5][0-9]{14}/,
    diners: /^3(?:0[0-5]|[68][0-9])[0-9]{11}/,
    amex: /^3[47][0-9]{13}/,
    discover: /^6(?:011|5[0-9]{2})[0-9]{12}/,
    hipercard: /^(606282\d{10}(\d{3})?)|(3841\d{15})/,
    elo: /^((((636368)|(438935)|(504175)|(451416)|(636297))\d{0,10})|((5067)|(4576)|(4011))\d{0,12})/,
    jcb: /^(?:2131|1800|35\d{3})\d{11}/,
    aura: /^(5078\d{2})(\d{2})(\d{11})$/,
  };

  // eslint-disable-next-line no-restricted-syntax
  for (const flag in cards) {
    if (cards[flag].test(cardnumberR)) {
      return flag;
    }
  }

  return false;
}

function getIconByFlag(width, height, flag) {
  if (flag && flag === "mastercard") {
    return <img alt="flag" src={MastercardIcon} style={{ width, height }} />;
  }
  if (flag && flag === "diners") {
    return <img alt="flag" source={DinersIcon} style={{ width, height }} />;
  }
  if (flag && flag === "amex") {
    return <img alt="flag" src={AmexIcon} style={{ width, height }} />;
  }
  if (flag && flag === "discover") {
    return <img alt="flag" src={DiscoverIcon} style={{ width, height }} />;
  }
  if (flag && flag === "jcb") {
    return <img alt="flag" src={JcbIcon} style={{ width, height }} />;
  }
  if (flag && flag === "elo") {
    return <img alt="flag" src={EloIcon} style={{ width, height }} />;
  }
  if (flag && flag === "visa") {
    return <img alt="flag" src={VisaIcon} style={{ width, height }} />;
  }
  if (flag && flag === "hipercard") {
    return <img alt="flag" src={HipercardIcon} style={{ width, height }} />;
  }
  return null;
}

function cardIcon({ numero, bandeira, width = 40, height = 25 }) {
  if (!numero || numero.length === 0) {
    return null;
  }
  const flag = getCardFlag(numero);
  return getIconByFlag(width, height, flag || bandeira);
}

export default {
  cardIcon,
  getIconByFlag,
  getCardFlag,
};
