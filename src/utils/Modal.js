import React from "react";
import { Modal } from "antd";
import { CloseCircleOutlined } from "@ant-design/icons";

const { confirm } = Modal;

const openDeleteModal = ({
  title,
  description,
  onOk,
  onCancel,
  okText = "Sim",
  cancelText = "Não",
  icon: Icon,
}) => {
  confirm({
    title,
    icon: Icon ? <Icon /> : <CloseCircleOutlined />,
    content: description,
    okText,
    okType: "danger",
    cancelText,
    onOk() {
      onOk && onOk();
    },
    onCancel() {
      onCancel && onCancel();
    },
  });
};

export default { openDeleteModal };
