import React from "react";
import { Row } from "../../common";
import { Colors, Metrics } from "../../config";

const styles = {
  title: {
    color: Colors.mainTheme.secondary,
    fontSize: Metrics.fontSize.lg,
    textTransform: "uppercase",
    fontWeight: "800",
  },
};

const BlockTitle = ({ title }) => (
  <Row>
    <h2 style={styles.title}>{title}</h2>
  </Row>
);

export default BlockTitle;
