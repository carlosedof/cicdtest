import React from "react";
import { Col } from "../../common";
import { Metrics } from "../../config";

const styles = {
  container: {
    paddingInline: Metrics.spacing.xxlg,
  },
};

const InternalPageLayout = ({ children }) => (
  <Col style={styles.container}>{children}</Col>
);

export default InternalPageLayout;
