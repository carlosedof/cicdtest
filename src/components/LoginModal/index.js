import React from "react";
import { Modal } from "antd";
import LoginForm from "../LoginForm";

const LoginModal = ({ visible, closeFn, setLoginVisible }) => (
  <Modal
    className="login-modal"
    visible={visible}
    footer={null}
    onCancel={closeFn}
  >
    <LoginForm setLoginVisible={setLoginVisible} />
  </Modal>
);
export default LoginModal;
