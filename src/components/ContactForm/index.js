import React from "react";
import { Formik } from "formik";
import { Input } from "formik-antd";
import { Button, Col, Row } from "../../common";
import { Colors, Metrics } from "../../config";
import { ContactValidationSchema } from "./ValidationSchema";

const { TextArea } = Input;

const styles = {
  footerBtnRow: {
    justifyContent: "flex-end",
    marginTop: Metrics.spacing.md,
  },
  footerFormContainer: {
    marginTop: Metrics.spacing.sm,
  },
  join: {
    backgroundColor: "rgba(255,255,255,0.8)",
    color: Colors.mainTheme.primary,
    borderRadius: "5px",
    border: 0,
    paddingInline: Metrics.spacing.xlg,
    width: "200px",
  },
  inputName: {
    borderColor: Colors.mainTheme.white,
    backgroundColor: "transparent",
    marginRight: Metrics.spacing.md,
  },
  inputEmail: {
    borderColor: Colors.mainTheme.white,
    backgroundColor: "transparent",
  },
};

const ContactForm = () => (
  /*  const [{ isFetching }, fetch] = useFetch({
    provider: () => {},
    param: "",
    requestOnMount: false,
    initialData: [],
    resultHandler: {
      success: () =>
        Toast.showSuccessMessage("Formulário de contato enviado com sucesso!"),
    },
  }); */

  <Formik
    validationSchema={ContactValidationSchema}
    initialValues={{ name: "", email: "", message: "" }}
    onSubmit={() => {}}
  >
    {({ values, handleSubmit, setFieldValue, resetForm }) => (
      <>
        <Col style={styles.footerFormContainer}>
          <Row>
            <Input
              value={values.name}
              onChange={(e) => setFieldValue("name", e.target.value)}
              className="antd-header-input"
              placeholder="Seu nome"
              allowClear
              style={styles.inputName}
            />
            <Input
              value={values.email}
              onChange={(e) => setFieldValue("email", e.target.value)}
              className="antd-header-input"
              placeholder="Seu e-mail"
              allowClear
              style={styles.inputEmail}
            />
          </Row>
          <TextArea
            value={values.message}
            onChange={(e) => setFieldValue("message", e.target.value)}
            className="antd-transparent-text-area"
            placeholder="Sua mensagem"
          />
          <Row style={styles.footerBtnRow}>
            <Button
              // loading={isFetching}
              style={styles.join}
              onClick={() => {
                handleSubmit();
                resetForm();
              }}
            >
              Enviar
            </Button>
          </Row>
        </Col>
      </>
    )}
  </Formik>
);
export default ContactForm;
