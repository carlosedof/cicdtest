import * as Yup from "yup";

export const ContactValidationSchema = Yup.object().shape({
  name: Yup.string().trim().required("O campo Nome é obrigatório."),
  email: Yup.string().trim().required("O campo E-mail é obrigatório."),
  message: Yup.string().trim().required("O campo Mensagem é obrigatório."),
});
