import React, { useCallback } from "react";
import { UploadOutlined } from "@ant-design/icons";
import { Upload } from "antd";
import { useFormikContext } from "formik";
import { Col, Row } from "../../common";

const { Dragger } = Upload;

const FormNameUploader = ({ name, list, direct }) => {
  const { setFieldValue, values } = useFormikContext();
  const toBase64 = (file) =>
    new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = (error) => reject(error);
    });
  const onChange = async ({ file }) => {
    // setFileToUpload((ft) => [...ft, file]);
    const base64 = await toBase64(file);
    if (list) {
      setFieldValue(name, [
        ...values[name],
        direct ? base64 : { base: base64 },
      ]);
    } else {
      setFieldValue(name, direct ? base64 : { base: base64 });
    }
  };
  const props = {
    beforeUpload: () => false,
    multiple: true,
    fileList: [],
  };

  const removeItem = useCallback(
    (value, i) => {
      if (value?.id) {
        setFieldValue(
          name,
          list ? values[name].filter((v) => v.id !== value.id) : null
        );
      } else {
        setFieldValue(
          name,
          list ? values[name].filter((v, y) => i !== y) : null
        );
      }
    },
    [setFieldValue, values, name]
  );

  const getImageName = useCallback(() => {
    if (values[name]?.base) {
      return values[name]?.base?.length > 200
        ? values[name].base
        : `/api/uploads/${values[name]?.base}`;
    }
    return values[name].length > 200
      ? values[name]
      : `/api/uploads/${values[name]}`;
  }, [values]);

  return (
    <Col>
      <Dragger {...props} onChange={onChange} accept=".png">
        <UploadOutlined />
        <span>Arraste para subir</span>
      </Dragger>
      {list ? (
        <Row>
          {values[name]?.map((f, i) => (
            <Col onClick={() => removeItem(f, i)}>
              <img
                key={i}
                style={{ width: "230px" }}
                src={f.base?.length > 200 ? f.base : `/api/uploads/${f.base}`}
                alt="print"
              />
            </Col>
          ))}
        </Row>
      ) : (
        <Row>
          {values[name] && (
            <Col onClick={() => removeItem(values[name], 0)}>
              <img
                style={{
                  width: "230px",
                  backgroundColor: values.color || "unset",
                }}
                src={getImageName()}
                alt="print"
              />
            </Col>
          )}
        </Row>
      )}
    </Col>
  );
};

export default FormNameUploader;
