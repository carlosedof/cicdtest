import React from "react";
import { useHistory } from "react-router-dom";
import { Col } from "../../common";
import { Colors, Metrics } from "../../config";

const styles = {
  block: {
    width: "155px",
    paddingBlock: Metrics.spacing.md,
    paddingInline: Metrics.spacing.lg,
    backgroundColor: Colors.mainTheme.white,
    marginInline: Metrics.spacing.md,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: "12px",
    boxShadow: "1px 1px 5px #cacaca",
    cursor: "pointer",
  },
  name: {
    fontSize: Metrics.fontSize.xsm,
    color: Colors.mainTheme.primary,
  },
};

const AccountOption = ({ data }) => {
  const { push } = useHistory();
  return (
    <Col style={styles.block} onClick={() => push(data.path)}>
      {/* <img src={data?.img} style={styles.icon} alt="party-icon-filter" /> */}
      {data?.icon}
      <span style={styles.name}>{data?.name}</span>
    </Col>
  );
};

export default AccountOption;
