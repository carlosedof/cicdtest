import React from "react";
import { Modal } from "antd";

const ForgotPasswordModal = ({ visible, closeFn }) => (
  <Modal
    className="login-modal"
    visible={visible}
    footer={null}
    onCancel={closeFn}
  >
    <span>forgotpassword</span>
  </Modal>
);
export default ForgotPasswordModal;
