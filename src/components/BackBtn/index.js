import React from "react";
import { FaChevronLeft } from "react-icons/all";
import { useHistory } from "react-router-dom";
import { Col, Row } from "../../common";
import { Colors, Metrics } from "../../config";
import "./styles.css";

const styles = {
  line: {},
  block: {
    overflow: "hidden",
    position: "relative",
    color: Colors.mainTheme.secondary,
    backgroundColor: Colors.mainTheme.white,
    opacity: 0.8,
    paddingBlock: Metrics.spacing.md,
    paddingInline: Metrics.spacing.xlg,
    cursor: "pointer",
    borderRadius: "8px",
    width: "80px",
    height: "40px",
    alignItems: "center",
    justifyContent: "center",
  },
  icon: {
    color: Colors.mainTheme.secondary,
  },
  back: {
    fontSize: Metrics.fontSize.xsm,
    textTransform: "uppercase",
    fontWeight: "700",
  },
};

const BackBtn = () => {
  const { goBack } = useHistory();
  return (
    <Row className="backBtn" style={styles.line}>
      <Row style={styles.block} onClick={goBack}>
        <Col>
          <FaChevronLeft style={styles.icon} />
        </Col>
        <span className="text" style={styles.back}>
          VOLTAR
        </span>
      </Row>
    </Row>
  );
};

export default BackBtn;
