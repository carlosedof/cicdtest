import React, { useCallback, useContext } from "react";
import { Formik } from "formik";
import { Button as BtnAntd } from "antd";
import { LockFilled, MailFilled } from "@ant-design/icons";
import { useHistory } from "react-router-dom";
import { Button, Col, FormInputText, Row } from "../../common";
import { Colors, Metrics } from "../../config";
import { LoginValidationSchema } from "./ValidationSchema";
import { AuthContext } from "../../context/AuthContext";
import { ROUTES_LIST } from "../../routes/ROUTES_LIST";
import BlockTitle from "../BlockTitle";

const styles = {
  container: {
    padding: Metrics.spacing.lg,
    alignItems: "center",
  },
  inputIcon: {
    marginRight: Metrics.spacing.xlg,
    color: Colors.mainTheme.secondary,
    fontSize: Metrics.fontSize.sm,
  },
  btnRow: {
    width: "100%",
    maxWidth: "200px",
    marginTop: Metrics.spacing.xxlg,
    marginBottom: Metrics.spacing.lg,
  },
  forgotRow: {
    width: "100%",
    marginTop: Metrics.spacing.md,
  },
  recover: {
    color: Colors.mainTheme.secondary,
    borderWidth: 0,
    padding: 0,
    boxShadow: "unset",
  },
  underlineText: {
    textDecoration: "underline",
  },
  noAccount: {
    fontSize: Metrics.fontSize.xsm,
  },
  register: {
    borderWidth: 0,
    color: Colors.mainTheme.secondary,
    marginTop: Metrics.spacing.sm,
    marginBottom: Metrics.spacing.xlg,
    boxShadow: "unset",
  },
  orEnter: {
    fontSize: Metrics.fontSize.xsm,
    marginBottom: Metrics.spacing.sm,
  },
  title: {
    color: Colors.mainTheme.secondary,
    fontSize: Metrics.fontSize.xlg,
    textTransform: "uppercase",
    fontFamily: "MADETommySoft-Bold",
  },
};

const LoginForm = ({ setLoginVisible }) => {
  const { push } = useHistory();
  const { handleLogin, isFetching } = useContext(AuthContext);

  const login = useCallback(async (values) => {
    await handleLogin(values);
    setLoginVisible(false);
  }, []);

  return (
    <Formik
      validationSchema={LoginValidationSchema}
      initialValues={{ email: "", password: "" }}
      // initialValues={{ email: "aa@a.com", password: "12345678" }}
      onSubmit={login}
    >
      {({ values, handleSubmit }) => (
        <>
          <Col style={styles.container}>
            <BlockTitle title="Acessar área de clientes" />
            <FormInputText
              fullWidth
              className="antd-login-input"
              value={values.email}
              name="email"
              placeholder="E-mail"
              style={styles.input}
              prefix={<MailFilled style={styles.inputIcon} />}
              allowClear
            />
            <FormInputText
              fullWidth
              className="antd-login-input"
              value={values.password}
              name="password"
              type="password"
              placeholder="Senha"
              style={styles.input}
              prefix={<LockFilled style={styles.inputIcon} />}
              allowClear
            />
            <Row style={styles.forgotRow}>
              <BtnAntd
                style={styles.recover}
                onClick={() => {
                  setLoginVisible(false);
                  push(ROUTES_LIST.FORGOT_PASSWORD.path);
                }}
              >
                <span style={styles.underlineText}>Recuperar a senha</span>
              </BtnAntd>
            </Row>
            <Row style={styles.btnRow} onClick={handleSubmit}>
              <Button loading={isFetching}>Entrar</Button>
            </Row>
          </Col>
          {/* <ErrorContainer /> */}
        </>
      )}
    </Formik>
  );
};

export default LoginForm;
