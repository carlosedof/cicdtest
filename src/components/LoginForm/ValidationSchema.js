import * as Yup from "yup";

export const LoginValidationSchema = Yup.object().shape({
  email: Yup.string().trim().required("O campo E-mail é obrigatório."),
  password: Yup.string().trim().required("O campo Senha é obrigatório."),
});
