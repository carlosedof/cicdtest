import React from "react";
import { Col } from "../../common";
import { Colors, Metrics } from "../../config";
import "./styles.css";

const styles = {
  card: {
    margin: Metrics.spacing.md,
    backgroundColor: Colors.mainTheme.white,
    boxShadow: "1px 1px 10px #e0e0e0",
    borderRadius: "10px",
    width: "210px",
    height: "270px",
    alignItems: "center",
    justifyContent: "space-around",
    padding: Metrics.spacing.md,
    backgroundSize: "cover",
    position: "relative",
    overflow: "hidden",
  },
  img: {
    objectFit: "cover",
    position: "absolute",
    top: 0,
    filter: "blur(4px) opacity(0.20)",
  },
  title: {
    fontFamily: "MADETommySoft-Bold",
    fontSize: Metrics.fontSize.sm,
    textAlign: "center",
    zIndex: "1",
    color: Colors.mainTheme.tertiary,
  },
  subtitle: {
    fontSize: Metrics.fontSize.xsm,
    fontWeight: "600",
    textAlign: "center",
    zIndex: "1",
    textShadow: "0px 0px 1px gray",
  },
};

const SolutionCard = ({ sl }) => (
  <Col className="solution-card" style={styles.card}>
    <img style={styles.img} src={sl.image} alt="card-bg" />
    <h3 style={styles.title}>{sl.title}</h3>
    <h4 style={styles.subtitle}>{sl.subtitle}</h4>
  </Col>
);

export default SolutionCard;
