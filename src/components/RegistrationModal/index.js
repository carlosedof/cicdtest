import React from "react";
import { Modal } from "antd";
import RegistrationForm from "../RegistrationForm";
import useBreakpoint from "../../hooks/useBreakpoint";

const styles = {
  modal: {
    maxWidth: "900px",
  },
};
const RegistrationModal = ({ visible, closeFn }) => {
  const { isMobile } = useBreakpoint();
  return (
    <Modal
      width={isMobile ? "95%" : "80%"}
      style={styles.modal}
      className="registration-modal"
      visible={visible}
      footer={null}
      onCancel={closeFn}
    >
      <RegistrationForm />
    </Modal>
  );
};
export default RegistrationModal;
