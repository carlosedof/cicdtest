import React from "react";
import { Modal } from "antd";

const ChangePasswordModal = ({ visible, closeFn }) => (
  <Modal
    className="login-modal"
    visible={visible}
    footer={null}
    onCancel={closeFn}
  >
    <span>CHANGEPWD</span>
  </Modal>
);
export default ChangePasswordModal;
