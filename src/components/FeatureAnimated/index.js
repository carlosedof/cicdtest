import React from "react";
import { Colors, Metrics } from "../../config";
import "./styles.css";

const styles = {
  coverLi: {
    maxWidth: "800px",
    lineHeight: "2",
    textAlign: "left",
    textTransform: "uppercase",
    fontFamily: "MADETommySoft-Bold",
    textShadow: "1px 1px 15px black",
    color: Colors.mainTheme.white,
    fontSize: Metrics.fontSize.lg,
  },
};

const FeatureAnimated = () => (
  <ul style={{ listStyleType: "none" }}>
    <li>
      <h1 className="animated-title fr1" style={styles.coverLi}>
        Softwares personalizados
      </h1>
    </li>
    <li>
      <h1 className="animated-title fr2" style={styles.coverLi}>
        Desenvolvimento de aplicativos, sistemas e produtos
      </h1>
    </li>
    <li>
      <h1 className="animated-title fr3" style={styles.coverLi}>
        Time confiável, experiente e comunicativo
      </h1>
    </li>
    <li>
      <h1 className="animated-title fr4" style={styles.coverLi}>
        Consultoria
      </h1>
    </li>
    <li>
      <h1 className="animated-title fr5" style={styles.coverLi}>
        Soluções tecnológicas
      </h1>
    </li>
  </ul>
);

export default FeatureAnimated;
