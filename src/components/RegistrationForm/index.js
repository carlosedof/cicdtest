import React, { useCallback, useContext } from "react";
import { Formik } from "formik";
import { Button as BtnAntd } from "antd";
import { LockOutlined, MailOutlined, UserOutlined } from "@ant-design/icons";
import { useHistory } from "react-router-dom";
import { Button, Col, FormInputText, Row } from "../../common";
import { Colors, Metrics } from "../../config";
import { LoginValidationSchema } from "./ValidationSchema";
import { AuthContext } from "../../context/AuthContext";
import { ROUTES_LIST } from "../../routes/ROUTES_LIST";
import BlockTitle from "../BlockTitle";

const styles = {
  container: {
    padding: Metrics.spacing.lg,
    alignItems: "center",
  },
  inputIcon: {
    marginRight: Metrics.spacing.xlg,
    color: Colors.mainTheme.secondary,
    fontSize: Metrics.fontSize.sm,
  },
  btnRow: {
    width: "100%",
    maxWidth: "200px",
    marginTop: Metrics.spacing.xxlg,
    marginBottom: Metrics.spacing.lg,
  },
  forgotRow: {
    width: "100%",
    marginTop: Metrics.spacing.md,
  },
  recover: {
    color: Colors.mainTheme.secondary,
    borderWidth: 0,
    padding: 0,
    boxShadow: "unset",
  },
  underlineText: {
    textDecoration: "underline",
  },
  noAccount: {
    fontSize: Metrics.fontSize.xsm,
  },
  register: {
    borderWidth: 0,
    color: Colors.mainTheme.secondary,
    marginTop: Metrics.spacing.sm,
    marginBottom: Metrics.spacing.xlg,
    boxShadow: "unset",
  },
  orEnter: {
    fontSize: Metrics.fontSize.xsm,
    marginBottom: Metrics.spacing.sm,
  },
  title: {
    color: Colors.mainTheme.secondary,
    fontSize: Metrics.fontSize.xlg,
    textTransform: "uppercase",
    fontFamily: "MADETommySoft-Bold",
  },
};

const RegistrationForm = ({ setLoginVisible }) => {
  const { push } = useHistory();
  const { handleLogin, isFetching } = useContext(AuthContext);

  const login = useCallback(async (values) => {
    await handleLogin(values);
    setLoginVisible(false);
  }, []);

  return (
    <Formik
      validationSchema={LoginValidationSchema}
      initialValues={{ email: "aa@a.com", password: "1234" }}
      onSubmit={login}
    >
      {({ values, handleSubmit }) => (
        <>
          <Col style={styles.container}>
            <BlockTitle title="Crie sua conta" />
            <Row
              responsive
              style={{ width: "100%", justifyContent: "space-between" }}
            >
              <Col style={{ width: "47%" }} fullWidthResponsive>
                <FormInputText
                  fullWidth
                  className="antd-login-input"
                  value={values.email}
                  name="email"
                  placeholder="Nome"
                  style={styles.input}
                  prefix={<UserOutlined style={styles.inputIcon} />}
                  allowClear
                />
              </Col>
              <Col style={{ width: "47%" }} fullWidthResponsive>
                <FormInputText
                  fullWidth
                  className="antd-login-input"
                  value={values.password}
                  name="password"
                  type="password"
                  placeholder="Senha"
                  style={styles.input}
                  prefix={<LockOutlined style={styles.inputIcon} />}
                  allowClear
                />
              </Col>
            </Row>
            <Row
              responsive
              style={{ width: "100%", justifyContent: "space-between" }}
            >
              <Col style={{ width: "47%" }} fullWidthResponsive>
                <FormInputText
                  fullWidth
                  className="antd-login-input"
                  value={values.email}
                  name="email"
                  placeholder="E-mail"
                  style={styles.input}
                  prefix={<MailOutlined style={styles.inputIcon} />}
                  allowClear
                />
              </Col>
              <Col style={{ width: "47%" }} fullWidthResponsive>
                <FormInputText
                  fullWidth
                  className="antd-login-input"
                  value={values.password}
                  name="password"
                  type="password"
                  placeholder="Confirme sua senha"
                  style={styles.input}
                  prefix={<LockOutlined style={styles.inputIcon} />}
                  allowClear
                />
              </Col>
            </Row>
            <span>
              Ao se cadastrar você concorda com os Termos de Uso e a Política de
              Privacidade
            </span>
            <Row style={styles.btnRow} onClick={handleSubmit}>
              <Button loading={isFetching}>Criar conta</Button>
            </Row>
            <span style={styles.noAccount}>Já possui uma conta?</span>
            <BtnAntd
              style={styles.register}
              onClick={() => {
                setLoginVisible(false);
                push(ROUTES_LIST.REGISTRATION.path);
              }}
            >
              <span style={styles.underlineText}>Fazer login</span>
            </BtnAntd>
            <span style={styles.orEnter}>Ou cadastre-se com</span>
            {/* <SocialBtns /> */}
          </Col>
          {/* <ErrorContainer /> */}
        </>
      )}
    </Formik>
  );
};

export default RegistrationForm;
