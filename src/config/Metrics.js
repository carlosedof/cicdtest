const headerHeight = "80px";
const footerHeight = 300;
const spacing = {
  minimum: 2,
  xsm: 4,
  sm: 8,
  md: 12,
  lg: 24,
  xlg: 36,
  xxlg: 45,
  xxxlg: 55,
};

const fontSize = {
  xxxxsm: 4,
  xxxsm: 6,
  xxsm: 8,
  xsm: 14,
  sm: 18,
  md: 23,
  lg: 25,
  xlg: 35,
  xxlg: 45,
  xxxlg: 50,
  xxxxlg: 65,
};

export default {
  fontSize,
  spacing,
  headerHeight,
  footerHeight,
};
