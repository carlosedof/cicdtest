const white = "#FFF";
const primary = "rgb(17,24,33)";
const secondary = "#738CB0";
const tertiary = "#0483c3";
const aux = "#5C6A79";
const gray = "#ececec5e";
const red = "#e24c55";

const mainTheme = {
  primary,
  secondary,
  tertiary,
  error: red,
  white,
  gray,
  aux,
};

export default {
  mainTheme,
};
