import { useEffect, useState } from "react";

const breakpoints = {
  0: "xs",
  600: "sm",
  960: "md",
  1150: "lg",
};

const useBreakpoint = () => {
  const [breakpoint, setBreakPoint] = useState("");
  const [windowSize, setWindowSize] = useState({
    width: undefined,
    height: undefined,
  });

  const handleResize = () => {
    setWindowSize({
      width: window.innerWidth,
      height: window.innerHeight,
    });
  };

  useEffect(() => {
    window.addEventListener("resize", handleResize);
    handleResize();

    if (windowSize.width > 0 && windowSize.width < 600) {
      setBreakPoint(breakpoints[0]);
    }
    if (windowSize.width > 600 && windowSize.width < 960) {
      setBreakPoint(breakpoints[600]);
    }
    if (windowSize.width > 960 && windowSize.width < 1150) {
      setBreakPoint(breakpoints[960]);
    }
    if (windowSize.width >= 1150) {
      setBreakPoint(breakpoints[1150]);
    }
    return () => window.removeEventListener("resize", handleResize);
  }, [windowSize.width]);
  return { isMobile: breakpoint !== "lg" && breakpoint !== "md" };
};

export default useBreakpoint;
