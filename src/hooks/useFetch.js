import { useEffect, useReducer, useRef } from "react";
import useCompare from "./useCompare";
import { Toast } from "../utils";

const initialState = {
  isFetching: false,
  filter: {},
  data: null,
  totalItems: 0,
};

const reducer = (state, action) => {
  switch (action.type) {
    case "FETCH_INIT":
      return { ...state, isFetching: true };
    case "FETCH_SUCCESS":
      return {
        ...state,
        isFetching: false,
        data: action.payload.data,
        totalItems: action.payload.totalItems,
      };
    case "FILTER":
      return {
        ...initialState,
        filter: action.payload.filter,
        data: action.payload.initialData,
        totalItems: action.payload.totalItems,
      };
    default:
      throw new Error();
  }
};

export default ({
  provider,
  param,
  requestOnMount,
  initialData,
  resultHandler,
}) => {
  const [state, dispatch] = useReducer(reducer, {
    ...initialState,
    data: initialData,
  });
  const diff = useCompare(param);
  const diffProvider = useCompare(provider);
  const shouldLoad = useRef(requestOnMount);
  const successHandler =
    resultHandler && resultHandler.success ? resultHandler.success : () => {};
  const errorHandler = (err) => {
    if (resultHandler && resultHandler.error) {
      resultHandler.error(err);
    } else {
      Toast.showErrorMessage(err?.message);
    }
    dispatch({ type: "FETCH_SUCCESS", payload: { data: initialData } });
    if (err?.message?.indexOf("400") === -1) {
      // NavigationService.navigate({name: AUTH_STACK_ROUTES.ERROR.name});
    }
  };

  const { filter } = state;

  useEffect(() => {
    if (diff || diffProvider) {
      dispatch({ type: "FETCH_SUCCESS", payload: { data: initialData } });
    } else if (shouldLoad.current) {
      dispatch({ type: "FETCH_INIT" });
      provider(param, filter)
        .then((result) => {
          dispatch({
            type: "FETCH_SUCCESS",
            payload: {
              data: result.status === 200 ? result?.data : initialData,
              totalItems: null,
            },
          });
          shouldLoad.current = false;
          if (result.status === 200 || result.status === 204) {
            successHandler(result);
          } else if (result.status !== 200) {
            errorHandler(result);
          }
        })
        .catch((err) => errorHandler(err));
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [diff, diffProvider, filter, provider, shouldLoad.current]);

  function fetchFilter(payload) {
    shouldLoad.current = true;
    dispatch({
      type: "FILTER",
      payload: {
        filter:
          typeof payload === "string" || typeof payload === "number"
            ? payload
            : { ...payload },
        initialData,
      },
    });
  }

  return [state, fetchFilter];
};
