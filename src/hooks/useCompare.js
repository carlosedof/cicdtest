import usePrevious from "./usePrevious";

export default (val) => {
  const prevVal = usePrevious(val);
  return prevVal !== val;
};
