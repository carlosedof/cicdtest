import { useEffect, useReducer, useCallback } from "react";
import api from "services/api";
import qs from "qs";

const INITIAL_STATE = {
  data: undefined,
  isLoading: false,
};

const reducer = (_, action) => {
  switch (action.type) {
    case "FETCH_INIT":
      return { data: undefined, isLoading: true };
    case "FETCH_SUCCESS":
      return { data: action.data, isLoading: false };
    default:
      throw new Error();
  }
};

export default function useDownload(url, initialData, fetchOnMount = true) {
  const [state, dispatch] = useReducer(reducer, {
    ...INITIAL_STATE,
  });

  const fetch = useCallback(
    (par) => {
      dispatch({ type: "FETCH_INIT" });
      const param = par;
      if (param.undefined) {
        delete param.undefined;
      }
      api
        .get(url, param, {
          paramsSerializer: (params) => qs.stringify(params),
          responseType: "blob",
        })
        .then((result) => {
          const contentType = result.headers["Content-Type"];
          const filename = result.headers.filename;
          const data = new Blob([result.data], { type: contentType });
          const urlObj = window.URL.createObjectURL(data);
          const tempLink = document.createElement("a");
          tempLink.href = urlObj;
          tempLink.setAttribute("download", filename);
          tempLink.click();
          dispatch({ type: "FETCH_SUCCESS", data: result.data });
        })
        .catch((e) => {
          // eslint-disable-next-line no-console
          console.log(e);
        });
    },
    [url]
  );

  useEffect(() => {
    if (fetchOnMount) {
      fetch();
    }
  }, [fetchOnMount, fetch]);

  return { loading: state.isLoading, data: state.data, fetch };
}
