import React from "react";
import { Col, Row } from "../../../common";
import { Colors, Metrics } from "../../../config";
import Dtdevs from "../../../assets/images/dtdevs-alter.png";
// import Fb from "../../../assets/images/facebook.png";
// import Ig from "../../../assets/images/ig.png";
// import Yt from "../../../assets/images/youtube.png";
// import Li from "../../../assets/images/linkedin.png";
import { ContactForm } from "../../../components";

const styles = {
  button: {
    backgroundColor: "transparent",
    border: 0,
    color: Colors.mainTheme.white,
    marginLeft: Metrics.spacing.xlg,
  },

  footerContainer: {
    minHeight: Metrics.footerHeight,
    background: Colors.mainTheme.tertiary,
  },
  footerImgText: {
    alignItems: "center",
    marginBottom: Metrics.spacing.xsm,
  },
  footerSide: {
    width: "50%",
    paddingInline: Metrics.spacing.xxxlg,
    paddingBlock: Metrics.spacing.lg,
  },
  logo: {
    width: "140px",
    height: "55px",
    marginRight: Metrics.spacing.xxlg,
  },
  footerTitle: {
    color: Colors.mainTheme.white,
    textTransform: "uppercase",
  },
  footerText: {
    color: Colors.mainTheme.white,
    fontSize: Metrics.fontSize.xsm,
    textAlign: "left",
    fontFamily: "Nunito",
  },
  socialIcon: {
    marginBlock: Metrics.spacing.md,
    marginRight: Metrics.spacing.sm,
    borderRadius: "7px",
  },
};

const Footer = () => (
  <Col style={styles.footerContainer}>
    <Row responsive fullWidthResponsive>
      <Col style={styles.footerSide} fullWidthResponsive>
        <Row style={styles.footerImgText}>
          <img src={Dtdevs} style={styles.logo} alt="logo-ph" />
          <Col>
            <h4 style={styles.footerTitle}>
              <b>Quem somos?</b>
            </h4>
            <span style={styles.footerText}>
              Somos uma equipe com larga experiência, excelência e
              comprometimento. Possuímos as mais atuais e consolidadas soluções
              do mercado, com participação no processo de desenvolvimento do
              início ao fim. Com entregas contínuas, feedbacks constantes,
              mantemos os clientes informados sobre o andamento do projeto.
              Priorizamos a boa comunicação, previsibilidade e responsabilidade
              em nossos projetos.
            </span>
          </Col>
        </Row>
        {/* <Row> */}
        {/*  <img style={styles.socialIcon} src={Fb} alt="fb" /> */}
        {/*  <img style={styles.socialIcon} src={Ig} alt="ig" /> */}
        {/*  <img style={styles.socialIcon} src={Yt} alt="yt" /> */}
        {/*  <img style={styles.socialIcon} src={Li} alt="li" /> */}
        {/* </Row> */}
      </Col>
      <Col style={styles.footerSide} fullWidthResponsive>
        <b style={styles.footerTitle}>Fale com a gente</b>
        <ContactForm />
      </Col>
    </Row>
    {/* <CardFlags /> */}
  </Col>
);

export default Footer;
