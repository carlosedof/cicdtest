import { Layout } from "antd";
import React from "react";
import { useLocation } from "react-router-dom";
import { Colors, Metrics } from "../../../config";
import { useWindowSize } from "../../../hooks/useWindowSize";
import Header from "./Header";
import Footer from "./Footer";

const styles = {
  layout: { backgroundImage: `unset` },
  button: {
    backgroundColor: "transparent",
    border: 0,
    color: Colors.mainTheme.white,
    marginLeft: Metrics.spacing.xlg,
  },
};

const CommonLayout = ({ children }) => {
  const { height } = useWindowSize();
  const { pathname } = useLocation();

  return (
    <Layout style={styles.layout}>
      <Header />
      <Layout
        style={{
          paddingTop: pathname === "/" ? 0 : Metrics.headerHeight,
          minHeight: +(height || 0) - +Metrics.footerHeight,
        }}
      >
        {children}
      </Layout>
      <Footer />
    </Layout>
  );
};

export default CommonLayout;
