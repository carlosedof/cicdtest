import React, { useCallback, useState } from "react";
import { useHistory } from "react-router-dom";
import { animateScroll, scroller } from "react-scroll";
import { useLocation } from "react-router";
import { Button, Col, Row } from "../../../common";
import { Colors, Metrics } from "../../../config";
import DtdevsLogo from "../../../assets/images/dtdevs-alter.png";
import { LoginModal } from "../../../components";
import { useAuthentication } from "../../../context/AuthContext";
import { ROUTES_LIST } from "../../../routes/ROUTES_LIST";

const styles = {
  layout: { backgroundImage: `unset` },
  button: {
    backgroundColor: "transparent",
    border: 0,
    width: "130px",
    color: Colors.mainTheme.white,
    marginLeft: Metrics.spacing.xlg,
  },
  join: {
    color: Colors.mainTheme.secondary,
    borderRadius: "5px",
    border: 0,
    paddingInline: Metrics.spacing.xlg,
  },
  cart: {
    marginLeft: Metrics.spacing.sm,
    color: Colors.mainTheme.secondary,
    backgroundColor: "transparent",
    borderRadius: "5px",
    border: 0,
  },
  headerBtns: {
    alignItems: "center",
  },
  headerContent: {
    zIndex: 5,
    justifyContent: "space-between",
    alignItems: "center",
    background: "transparent",
    color: "#03a28a",
    height: Metrics.headerHeight,
    width: "100%",
    position: "absolute",
    paddingInline: Metrics.spacing.xlg,
    paddingBlock: Metrics.spacing.sm,
  },
  headerBg: {
    zIndex: 4,
    justifyContent: "space-between",
    background: "#0483c3",
    color: "#03a28a",
    height: "800px",
    width: "100%",
    position: "absolute",
    transition: "all 0.5s ease",
  },
  headerContainer: {
    zIndex: 4,
    position: "fixed",
    top: 0,
    height: Metrics.headerHeight,
    width: "100%",
    overflow: "hidden",
  },
  logo: {
    width: "140px",
    height: "55px",
    marginRight: Metrics.spacing.md,
  },
};

const Header = () => {
  const [loginVisible, setLoginVisible] = useState();
  // const { authenticated, handleLogout } = useAuthentication();
  const { pathname } = useLocation();
  const { push } = useHistory();
  const { authenticated, handleLogout } = useAuthentication();

  const handleEnterBtn = useCallback(() => {
    if (!authenticated) {
      setLoginVisible(true);
    } else {
      animateScroll.scrollToTop();
      push(ROUTES_LIST.ACCOUNT.path);
    }
  }, [authenticated, handleLogout, setLoginVisible]);

  return (
    <Col style={styles.headerContainer}>
      <LoginModal
        setLoginVisible={setLoginVisible}
        visible={loginVisible && !authenticated}
        closeFn={() => setLoginVisible(false)}
      />
      <Row style={styles.headerBg} />
      <Row style={styles.headerContent}>
        <Row responsive style={styles.headerBtns}>
          <img src={DtdevsLogo} alt="logo-ph" style={styles.logo} />
          <Button
            className="hoverable-grow-btn"
            style={styles.button}
            onClick={() => {
              if (pathname !== "/") {
                push("/");
              }
              setTimeout(
                () => animateScroll.scrollToTop(),
                pathname !== "/" ? 500 : 0
              );
            }}
          >
            Início
          </Button>
          <Button
            className="hoverable-grow-btn"
            style={styles.button}
            onClick={() => {
              if (pathname !== "/") {
                push("/");
              }
              setTimeout(
                () =>
                  scroller.scrollTo("portfolio", {
                    duration: 800,
                    delay: 0,
                    smooth: "easeInOutQuart",
                    offset: -100,
                  }),
                pathname !== "/" ? 500 : 0
              );
            }}
          >
            Portfólio
          </Button>
          <Button
            className="hoverable-grow-btn"
            style={styles.button}
            onClick={() => {
              if (pathname !== "/") {
                push("/");
              }
              setTimeout(
                () =>
                  scroller.scrollTo("solutions", {
                    duration: 800,
                    delay: 0,
                    smooth: "easeInOutQuart",
                    offset: -100,
                  }),
                pathname !== "/" ? 500 : 0
              );
            }}
          >
            Soluções
          </Button>
          <Button
            className="hoverable-grow-btn"
            style={styles.button}
            onClick={() => {
              if (pathname !== "/") {
                push("/");
              }
              setTimeout(
                () => animateScroll.scrollToBottom(),
                pathname !== "/" ? 500 : 0
              );
            }}
          >
            Quem somos?
          </Button>
          <Button
            className="hoverable-grow-btn"
            style={styles.button}
            onClick={() => {
              if (pathname !== "/") {
                push("/");
              }
              setTimeout(
                () => animateScroll.scrollToBottom(),
                pathname !== "/" ? 500 : 0
              );
            }}
          >
            Contato
          </Button>
        </Row>
        <Row>
          <Button
            className="hoverable-white-btn"
            style={styles.join}
            onClick={handleEnterBtn}
          >
            {authenticated ? "Área adm" : "Já sou cliente"}
          </Button>
        </Row>
      </Row>
    </Col>
  );
};

export default Header;
