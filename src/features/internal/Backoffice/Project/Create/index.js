import React from "react";
import { useHistory } from "react-router-dom";
import useFetch from "../../../../../hooks/useFetch";
import { Row } from "../../../../../common";
import ProjectForm from "../Form";
import BackBtn from "../../../../../components/BackBtn";
import { createProjectRequest } from "../../../../../services/data";
import { Metrics } from "../../../../../config";
import { InternalPageLayout } from "../../../../../components";

const ProjectCreate = () => {
  const { goBack } = useHistory();
  const [{ isFetching: isFetchingCreate }, createProject] = useFetch({
    provider: createProjectRequest,
    param: "",
    requestOnMount: false,
    initialData: [],
    resultHandler: {
      success: () => {
        goBack();
      },
    },
  });
  return (
    <InternalPageLayout>
      <Row style={{ alignItems: "center", marginBlock: Metrics.spacing.md }}>
        <BackBtn />
        <span
          style={{
            marginLeft: Metrics.spacing.md,
            fontSize: Metrics.fontSize.sm,
          }}
        >
          <b>Criação de Projeto</b>
        </span>
      </Row>
      {console.log(isFetchingCreate)}
      <ProjectForm onSubmit={createProject} />
    </InternalPageLayout>
  );
};

export default ProjectCreate;
