import React, { useState } from "react";
import { Input } from "formik-antd";
import { Formik } from "formik";
import { Select } from "antd";
import useFetch from "../../../../../hooks/useFetch";
import { findLanguagesRequest } from "../../../../../services/language";
import { Button, Col, FormCheckbox, Row } from "../../../../../common";
import { Colors, Metrics } from "../../../../../config";
import { FormNameUploader } from "../../../../../components";

const { TextArea } = Input;
const ProjectForm = ({ initialValues, onSubmit }) => {
  const [selectedLanguage, setSelectedLanguage] = useState();
  const [{ isFetching, data: languages }] = useFetch({
    provider: findLanguagesRequest,
    param: "",
    requestOnMount: true,
    initialData: [],
    resultHandler: {
      success: () => {},
    },
  });

  return (
    <Col>
      {console.log(isFetching)}
      <Formik
        enableReinitialize
        initialValues={
          initialValues || {
            title: "",
            color: "",
            company: "",
            description: "",
            logo: "",
            publishedAppstore: false,
            publishedPlaystore: false,
            isMobile: false,
            isWeb: false,
            solo: false,
            images: [],
            projectLanguages: [],
          }
        }
        onSubmit={onSubmit}
      >
        {({ values, handleSubmit, setFieldValue }) => (
          <>
            <Col style={{ marginInline: Metrics.spacing.md }}>
              <Input
                style={{ marginBottom: Metrics.spacing.md }}
                value={values.title}
                onChange={(e) => setFieldValue("title", e.target.value)}
                placeholder="Título"
                allowClear
              />
              <Input
                style={{ marginBottom: Metrics.spacing.md }}
                value={values.company}
                onChange={(e) => setFieldValue("company", e.target.value)}
                placeholder="Empresa"
                allowClear
              />
              <Input
                style={{ marginBottom: Metrics.spacing.md }}
                value={values.url}
                onChange={(e) => setFieldValue("url", e.target.value)}
                placeholder="URL"
                allowClear
              />
              <TextArea
                style={{ marginBottom: Metrics.spacing.md }}
                value={values.description}
                onChange={(e) => setFieldValue("description", e.target.value)}
                placeholder="Descrição"
              />
              <span
                style={{
                  marginBlock: Metrics.spacing.md,
                }}
              >
                <b>Logo</b> (clique na imagem para remover)
              </span>
              <FormNameUploader name="logo" />
              <Row>
                <input
                  type="color"
                  value={values.color}
                  onChange={(e) => setFieldValue("color", e.target.value)}
                />
                <Button min onClick={() => setFieldValue("color", "")}>
                  Limpar cor
                </Button>
              </Row>
              <Col>
                <FormCheckbox
                  style={{ marginBottom: Metrics.spacing.md }}
                  label="Publicado na appstore"
                  name="publishedAppstore"
                  value={values?.publishedAppstore}
                />
                <FormCheckbox
                  style={{ marginBottom: Metrics.spacing.md }}
                  label="Publicado na playstore"
                  name="publishedPlaystore"
                  value={values?.publishedPlaystore}
                />
                <FormCheckbox
                  style={{ marginBottom: Metrics.spacing.md }}
                  label="É mobile"
                  name="isMobile"
                  value={values?.isMobile}
                />
                <FormCheckbox
                  style={{ marginBottom: Metrics.spacing.md }}
                  label="É web"
                  name="isWeb"
                  value={values?.isWeb}
                />
                <FormCheckbox
                  style={{ marginBottom: Metrics.spacing.md }}
                  label="Desenvolvido inteiramente por dtdevs"
                  name="solo"
                  value={values?.solo}
                />
              </Col>
              <span
                style={{
                  marginBlock: Metrics.spacing.md,
                }}
              >
                <b>Prints do projeto</b> (clique na imagem para remover)
              </span>
              <FormNameUploader name="images" list />
              <Col>
                <span
                  style={{
                    marginBlock: Metrics.spacing.md,
                  }}
                >
                  <b>Linguagens selecionadas</b> (clique na imagem para remover)
                </span>
                <Row style={{ alignItems: "center" }}>
                  {values?.projectLanguages?.length === 0 && (
                    <Col
                      style={{
                        boxShadow: "#bfbfbf 1px 1px 15px",
                        borderRadius: "12px",
                        width: "50px",
                        height: "50px",
                        backgroundColor: Colors.mainTheme.gray,
                        justifyContent: "center",
                        alignItems: "center",
                        color: Colors.mainTheme.tertiary,
                        fontSize: Metrics.fontSize.xsm,
                      }}
                    >
                      LOGO
                    </Col>
                  )}
                  {values.projectLanguages.map((l) => (
                    <Col
                      style={{ marginInline: Metrics.spacing.sm }}
                      onClick={() =>
                        setFieldValue(
                          "projectLanguages",
                          values.projectLanguages.filter(
                            (pl) => pl?.id !== l?.id
                          )
                        )
                      }
                    >
                      <img
                        style={{ width: "50px" }}
                        src={l?.language?.icon || l?.icon}
                        alt="langs"
                      />
                    </Col>
                  ))}
                </Row>
              </Col>
              <Col>
                {console.log(languages)}
                {console.log(values.projectLanguages)}
                <Col>
                  <span
                    style={{
                      fontWeight: "bolder",
                      marginBlock: Metrics.spacing.md,
                    }}
                  >
                    Linguagens disponiveis
                  </span>
                  <Row style={{ marginBlock: Metrics.spacing.md }}>
                    <Select
                      value={selectedLanguage?.id}
                      style={{ width: 240, marginRight: Metrics.spacing.md }}
                      onChange={(e) =>
                        setSelectedLanguage(
                          languages?.find((lan) => lan?.id === e)
                        )
                      }
                    >
                      {languages
                        .filter(
                          (ll) =>
                            !values.projectLanguages.find(
                              (la) => la?.id === ll?.id
                            )
                        )
                        .map((l) => (
                          <Select.Option value={l?.id}>{l?.name}</Select.Option>
                        ))}
                    </Select>
                    <Button
                      disabled={!selectedLanguage}
                      min
                      onClick={() => {
                        setFieldValue("projectLanguages", [
                          ...values.projectLanguages,
                          selectedLanguage,
                        ]);
                        setSelectedLanguage(null);
                      }}
                    >
                      Adicionar
                    </Button>
                  </Row>
                </Col>
              </Col>
              <Row style={{ marginBlock: Metrics.spacing.md }}>
                <Button
                  // loading={isFetching}
                  onClick={() => {
                    handleSubmit();
                    // resetForm();
                  }}
                >
                  Enviar
                </Button>
              </Row>
            </Col>
          </>
        )}
      </Formik>
    </Col>
  );
};
export default ProjectForm;
