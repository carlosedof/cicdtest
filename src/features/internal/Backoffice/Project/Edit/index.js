import React from "react";
import { useParams } from "react-router";
import { useHistory } from "react-router-dom";
import useFetch from "../../../../../hooks/useFetch";
import { Row } from "../../../../../common";
import BackBtn from "../../../../../components/BackBtn";
import {
  findProjectRequest,
  updateProjectRequest,
} from "../../../../../services/data";
import ProjectForm from "../Form";
import { Metrics } from "../../../../../config";
import { InternalPageLayout } from "../../../../../components";

const ProjectEdit = () => {
  const { id } = useParams();
  const { goBack } = useHistory();
  const [{ isFetching: isFetchingUpdate }, updateRecord] = useFetch({
    provider: updateProjectRequest,
    param: id,
    requestOnMount: false,
    initialData: null,
    resultHandler: {
      success: () => {
        goBack();
      },
    },
  });
  const [{ isFetching, data }] = useFetch({
    provider: findProjectRequest,
    param: id,
    requestOnMount: true,
    initialData: null,
    resultHandler: {
      success: () => {},
    },
  });
  return (
    <InternalPageLayout>
      <Row style={{ alignItems: "center", marginBlock: Metrics.spacing.md }}>
        <BackBtn />
        <span
          style={{
            marginLeft: Metrics.spacing.md,
            fontSize: Metrics.fontSize.sm,
          }}
        >
          <b>Edição de Projeto</b>
        </span>
      </Row>
      {console.log(isFetching)}
      {console.log(isFetchingUpdate)}
      {!isFetching && (
        <ProjectForm onSubmit={updateRecord} initialValues={data} />
      )}
    </InternalPageLayout>
  );
};
export default ProjectEdit;
