import React, { useCallback } from "react";
import { useHistory } from "react-router";
import { Dropdown, Menu, Table } from "antd";
import { AiOutlineMore } from "react-icons/all";
import { Button, Col, Row } from "../../../../common";
import useFetch from "../../../../hooks/useFetch";
import {
  deleteLanguageRequest,
  findLanguagesRequest,
} from "../../../../services/language";
import { ROUTES_LIST } from "../../../../routes/ROUTES_LIST";
import { Modal } from "../../../../utils";
import { Metrics } from "../../../../config";
import { InternalPageLayout } from "../../../../components";

const { Column } = Table;

const Language = () => {
  const { push } = useHistory();
  const [{ isFetching, data }, findAll] = useFetch({
    provider: findLanguagesRequest,
    param: "",
    requestOnMount: true,
    initialData: [],
    resultHandler: {
      success: () => {},
    },
  });
  const [{ isFetching: isFetchingDeleting }, deleteRecord] = useFetch({
    provider: deleteLanguageRequest,
    param: "",
    requestOnMount: false,
    initialData: [],
    resultHandler: {
      success: () => {
        findAll();
      },
    },
  });
  const menu = useCallback(
    (record) => (
      <Menu>
        <Menu.Item
          key="0"
          onClick={() =>
            push(`${ROUTES_LIST.LANGUAGE_EDIT.pathWithoutId}/${record.id}`)
          }
        >
          Editar
        </Menu.Item>
        <Menu.Item
          key="1"
          danger
          onClick={() =>
            Modal.openDeleteModal({
              description: (
                <span>
                  Deseja realmente deletar o registro
                  <strong> {record.name}</strong>?
                </span>
              ),
              onOk: () => deleteRecord(record.id),
              okText: "Sim",
              cancelText: "Não",
            })
          }
        >
          Excluir
        </Menu.Item>
      </Menu>
    ),
    [deleteRecord, push]
  );

  return (
    <InternalPageLayout>
      {console.log(isFetching)}
      {console.log(isFetchingDeleting)}
      <Row
        style={{
          justifyContent: "space-between",
          alignItems: "center",
          marginBlock: Metrics.spacing.md,
        }}
      >
        <span
          style={{
            fontSize: Metrics.fontSize.sm,
          }}
        >
          <b>Linguagens/Tecnologias</b>
        </span>
        <Button min onClick={() => push(ROUTES_LIST.LANGUAGE_CREATE.path)}>
          Novo
        </Button>
      </Row>
      <Col>
        <Table dataSource={data}>
          <Column title="Nome" dataIndex="name" key="name" />
          <Column
            title="Ícone"
            dataIndex="icon"
            key="icon"
            render={(_, record) => (
              <img
                style={{ width: "30px" }}
                src={
                  record.icon.length > 200
                    ? record.icon
                    : `/api/uploads/${record.icon}`
                }
                alt="icon"
              />
            )}
          />
          <Column
            title="Ações"
            width={80}
            align="center"
            render={(text, record) => (
              <span>
                <Dropdown
                  overlay={menu(record)}
                  placement="bottomRight"
                  trigger={["click"]}
                >
                  <AiOutlineMore size={25} style={{ cursor: "pointer" }} />
                </Dropdown>
              </span>
            )}
          />
        </Table>
      </Col>
    </InternalPageLayout>
  );
};
export default Language;
