import React from "react";
import { useHistory } from "react-router-dom";
import useFetch from "../../../../../hooks/useFetch";
import { createLanguageRequest } from "../../../../../services/language";
import { Row } from "../../../../../common";
import LanguageForm from "../Form";
import BackBtn from "../../../../../components/BackBtn";
import { Metrics } from "../../../../../config";
import { InternalPageLayout } from "../../../../../components";

const LanguageCreate = () => {
  const { goBack } = useHistory();
  const [{ isFetching: isFetchingCreate }, createLanguage] = useFetch({
    provider: createLanguageRequest,
    param: "",
    requestOnMount: false,
    initialData: [],
    resultHandler: {
      success: () => {
        goBack();
      },
    },
  });
  return (
    <InternalPageLayout>
      <Row style={{ alignItems: "center", marginBlock: Metrics.spacing.md }}>
        <BackBtn />
        <span
          style={{
            marginLeft: Metrics.spacing.md,
            fontSize: Metrics.fontSize.sm,
          }}
        >
          <b>Criação de Linguagem/Tecnologia</b>
        </span>
      </Row>
      {console.log(isFetchingCreate)}
      <LanguageForm onSubmit={createLanguage} />
    </InternalPageLayout>
  );
};

export default LanguageCreate;
