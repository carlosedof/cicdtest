import React from "react";
import { Input } from "formik-antd";
import { Formik } from "formik";
import { Button, Col, Row } from "../../../../../common";
import { Metrics } from "../../../../../config";
import { FormNameUploader } from "../../../../../components";

const LanguageForm = ({ onSubmit, initialValues }) => (
  <Col>
    <Formik
      initialValues={initialValues || { name: "", icon: "" }}
      onSubmit={onSubmit}
    >
      {({ values, handleSubmit, setFieldValue }) => (
        <>
          <Col style={{ marginInline: Metrics.spacing.md }}>
            <Input
              style={{ marginBottom: Metrics.spacing.md }}
              value={values.name}
              onChange={(e) => setFieldValue("name", e.target.value)}
              placeholder="name"
              allowClear
            />
            <FormNameUploader name="icon" direct />
            <Row style={{ marginBlock: Metrics.spacing.md }}>
              <Button
                // loading={isFetching}
                onClick={() => {
                  handleSubmit();
                  // resetForm();
                }}
              >
                Enviar
              </Button>
            </Row>
          </Col>
        </>
      )}
    </Formik>
  </Col>
);
export default LanguageForm;
