import React from "react";
import { useParams } from "react-router";
import { useHistory } from "react-router-dom";
import useFetch from "../../../../../hooks/useFetch";
import {
  findLanguageRequest,
  updateLanguageRequest,
} from "../../../../../services/language";
import LanguageForm from "../Form";
import { Row } from "../../../../../common";
import BackBtn from "../../../../../components/BackBtn";
import { Metrics } from "../../../../../config";
import { InternalPageLayout } from "../../../../../components";

const LanguageEdit = () => {
  const { goBack } = useHistory();
  const { id } = useParams();
  const [{ isFetching: isFetchingUpdate }, updateLanguage] = useFetch({
    provider: updateLanguageRequest,
    param: id,
    requestOnMount: false,
    initialData: null,
    resultHandler: {
      success: () => {
        goBack();
      },
    },
  });
  const [{ isFetching, data }] = useFetch({
    provider: findLanguageRequest,
    param: id,
    requestOnMount: true,
    initialData: null,
    resultHandler: {
      success: () => {},
    },
  });
  return (
    <InternalPageLayout>
      {console.log(isFetching)}
      {console.log(isFetchingUpdate)}
      <Row style={{ alignItems: "center", marginBlock: Metrics.spacing.md }}>
        <BackBtn />
        <span
          style={{
            marginLeft: Metrics.spacing.md,
            fontSize: Metrics.fontSize.sm,
          }}
        >
          <b>Edição de Linguagem/Tecnologia</b>
        </span>
      </Row>
      {!isFetching && (
        <LanguageForm onSubmit={updateLanguage} initialValues={data} />
      )}
    </InternalPageLayout>
  );
};

export default LanguageEdit;
