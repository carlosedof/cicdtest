import React from "react";
import {
  FileWordOutlined,
  FolderOpenFilled,
  LockOutlined,
  LogoutOutlined,
} from "@ant-design/icons";
import { Col, Row } from "../../../common";
import { Colors, Metrics } from "../../../config";
import { useAuthentication } from "../../../context/AuthContext";
import { AccountOption, BlockTitle } from "../../../components";
import { ROUTES_LIST } from "../../../routes/ROUTES_LIST";

const styles = {
  container: {
    marginTop: Metrics.headerHeight,
    justifyContent: "center",
    alignItems: "center",
  },
  icon: {
    marginBlock: Metrics.spacing.lg,
    color: Colors.mainTheme.secondary,
    fontSize: Metrics.fontSize.xlg,
  },
};

const Account = () => {
  const { user } = useAuthentication();
  return (
    <Col style={styles.container}>
      <BlockTitle title={`Olá, ${user?.name}`} />
      <Row>
        {[
          // {
          //   name: "Meus ingressos",
          //   img: Tickets,
          //   path: ROUTES_LIST.TICKETS.path,
          // },
          // { name: "Cartões", img: Cards, path: ROUTES_LIST.CARDS.path },
          {
            name: "Projetos",
            icon: <FolderOpenFilled style={styles.icon} />,
            path: ROUTES_LIST.PROJECT.path,
          },
          {
            name: "Linguagens/Tecnologias",
            icon: <FileWordOutlined style={styles.icon} />,
            path: ROUTES_LIST.LANGUAGE.path,
          },
          {
            name: "Alterar senha",
            icon: <LockOutlined style={styles.icon} />,
            path: ROUTES_LIST.CHANGE_PASSWORD.path,
          },
          {
            name: "Sair",
            icon: <LogoutOutlined style={styles.icon} />,
            path: ROUTES_LIST.LOGOUT.path,
          },
        ].map((ao) => (
          <AccountOption data={ao} />
        ))}
      </Row>
    </Col>
  );
};

export default Account;
