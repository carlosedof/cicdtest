import { useEffect } from "react";
import { useHistory } from "react-router-dom";
import { useAuthentication } from "../../../context/AuthContext";
import { ROUTES_LIST } from "../../../routes/ROUTES_LIST";

const Logout = () => {
  const { push } = useHistory();
  const { handleLogout } = useAuthentication();

  useEffect(() => {
    async function logout() {
      await handleLogout();
      push(ROUTES_LIST.HOME.path);
    }
    logout();
  }, [handleLogout]);

  return null;
};

export default Logout;
