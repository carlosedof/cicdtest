import React from "react";
import { useHistory } from "react-router-dom";
import { Col } from "../../../common";
import { Metrics } from "../../../config";
import { ChangePasswordModal } from "../../../components";

const styles = {
  container: {
    marginTop: Metrics.headerHeight,
    justifyContent: "center",
    alignItems: "center",
  },
};

const ChangePassword = () => {
  const { goBack } = useHistory();
  return (
    <Col style={styles.container}>
      <ChangePasswordModal visible closeFn={goBack} />
    </Col>
  );
};

export default ChangePassword;
