import React from "react";
import { useHistory } from "react-router-dom";
import { Col } from "../../../common";
import { Metrics } from "../../../config";
import { ForgotPasswordModal } from "../../../components";

const styles = {
  container: {
    marginTop: Metrics.headerHeight,
    justifyContent: "center",
    alignItems: "center",
  },
};

const ForgotPassword = () => {
  const { goBack } = useHistory();
  return (
    <Col style={styles.container}>
      <ForgotPasswordModal visible closeFn={goBack} />
    </Col>
  );
};

export default ForgotPassword;
