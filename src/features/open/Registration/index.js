import React from "react";
import { useHistory } from "react-router-dom";
import { Col } from "../../../common";
import { Metrics } from "../../../config";
import { RegistrationModal } from "../../../components";

const styles = {
  container: {
    marginTop: Metrics.headerHeight,
    justifyContent: "center",
    alignItems: "center",
  },
};

const Registration = () => {
  const { goBack } = useHistory();
  return (
    <Col style={styles.container}>
      <RegistrationModal visible closeFn={goBack} />
    </Col>
  );
};

export default Registration;
