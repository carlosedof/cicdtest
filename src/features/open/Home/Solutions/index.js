import React from "react";
import { Col, Row } from "../../../../common";
import { Metrics } from "../../../../config";
import MaintenanceImg from "../../../../assets/images/maintenance.png";
import AppsWebImg from "../../../../assets/images/appswebsys.png";
import ConsultImg from "../../../../assets/images/consult.png";
import PublishImg from "../../../../assets/images/publish.png";
import CustomSysImg from "../../../../assets/images/customsys.png";
import { BlockTitle, SolutionCard } from "../../../../components";

const styles = {
  container: {
    marginBottom: Metrics.spacing.xxxlg,
    marginLeft: Metrics.spacing.lg,
  },
  line: {
    justifyContent: "space-around",
    flexWrap: "wrap",
  },
};

const SOLUTIONS_LIST = [
  {
    title: "aplicativos e sistemas web",
    subtitle:
      "utilizamos as tecnologias mais atuais e consolidadas do mercado, para desenvolver os sistemas nas plataformas desejadas",
    image: AppsWebImg,
  },
  {
    title: "sistemas personalizados",
    subtitle:
      "desenvolvemos seu sistema do zero de acordo com sua necessidade, com menor custo e gasto de tempo possível",
    image: CustomSysImg,
  },
  {
    title: "melhorias e correções de sistemas existentes",
    subtitle:
      "melhoramos e corrigimos o sistema que sua empresa já utliza, entre em contato conosco para uma análise de viabilidade",
    image: MaintenanceImg,
  },
  {
    title: "publicação de aplicativos",
    subtitle:
      "publicamos seu aplicativo na appstore (apple/ios) e/ou na playstore (google/android), temos experiência para te auxiliar até a etapa final da publicação",
    image: PublishImg,
  },
  {
    title: "consultoria",
    subtitle:
      "analisamos seu negócio para encontrar as melhores soluções tecnologicas do mercado",
    image: ConsultImg,
  },
];

const Solutions = () => (
  <Col name="solutions" style={styles.container}>
    <BlockTitle title="Soluções" />
    <Row style={styles.line}>
      {SOLUTIONS_LIST.map((sl) => (
        <SolutionCard key={sl.title} sl={sl} />
      ))}
    </Row>
  </Col>
);

export default Solutions;
