import React from "react";
import { Fade } from "react-slideshow-image";
import { Col } from "../../../common";
import { Metrics } from "../../../config";
import a from "../../../assets/images/wall/alesia-kazantceva-XLm6-fPwK5Q-unsplash.jpg";
import b from "../../../assets/images/wall/christopher-gower-vjMgqUkS8q8-unsplash.jpg";
import c from "../../../assets/images/wall/carl-heyerdahl-KE0nC8-58MQ-unsplash.jpg";
import Portfolio from "./Portfolio";
import Solutions from "./Solutions";
import POC from "./POC";
import { FeatureAnimated } from "../../../components";

const styles = {
  container: {
    paddingTop: Metrics.headerHeight,
  },
};

const Home = () => {
  const fadeImages = [
    {
      url: a,
      caption: "First Slide",
    },
    {
      url: b,
      caption: "Second Slide",
    },
    {
      url: c,
      caption: "Third Slide",
    },
  ];
  return (
    <Col style={styles.container}>
      <div className="slide-container" style={{ position: "relative" }}>
        <Fade pauseOnHover={false} arrows={false} autoplay>
          {fadeImages.map((fadeImage, index) => (
            <div className="each-fade" key={index}>
              <div className="image-container">
                <img
                  style={{
                    width: "100%",
                    height: "700px",
                    objectFit: "cover",
                    filter:
                      "blur(6px) saturate(10.5) grayscale(0.6) sepia(0.2)",
                  }}
                  alt="img"
                  src={fadeImage.url}
                />
              </div>
            </div>
          ))}
        </Fade>
        <Col
          style={{
            position: "absolute",
            justifyContent: "center",
            alignItems: "center",
            width: "100%",
            height: "100%",
            zIndex: 1,
            top: 0,
          }}
        >
          <FeatureAnimated />
        </Col>
      </div>
      <Portfolio />
      <Solutions />
      <POC />
    </Col>
  );
};

export default Home;
