import React from "react";
import Carousel from "react-multi-carousel";
import { useHistory } from "react-router";
import { Col, Row } from "../../../../common";
import { Colors, Metrics } from "../../../../config";
import { findProjectsRequest } from "../../../../services/data";
import { ROUTES_LIST } from "../../../../routes/ROUTES_LIST";
import useFetch from "../../../../hooks/useFetch";
import { BlockTitle } from "../../../../components";

const styles = {
  container: {
    marginTop: Metrics.spacing.lg,
    marginLeft: Metrics.spacing.lg,
  },
};

const Portfolio = () => {
  const [{ data }] = useFetch({
    provider: findProjectsRequest,
    param: "",
    requestOnMount: true,
    initialData: [],
    resultHandler: {
      success: () => {},
    },
  });
  const { push } = useHistory();
  const responsive = {
    superLargeDesktop: {
      // the naming can be any, depends on you.
      breakpoint: { max: 4000, min: 3000 },
      items: 6,
    },
    desktop: {
      breakpoint: { max: 3000, min: 1024 },
      items: 5,
    },
    tablet: {
      breakpoint: { max: 1024, min: 464 },
      items: 4,
    },
    mobile: {
      breakpoint: { max: 464, min: 0 },
      items: 1,
    },
  };

  return (
    <Col name="portfolio" style={styles.container}>
      <BlockTitle title="Portfólio" />
      <Col
        style={{
          marginBlock: Metrics.spacing.xlg,
          paddingBlock: Metrics.spacing.md,
        }}
      >
        <Carousel
          responsive={responsive}
          autoPlay
          infinite
          autoPlaySpeed={1600}
        >
          {data.map((p, i) => (
            <Col
              onClick={() =>
                push(
                  `${ROUTES_LIST.PROJECT_PRESENTATION.pathWithoutId}/${p.id}`
                )
              }
              key={i}
              style={{
                padding: Metrics.spacing.xlg,
              }}
            >
              <Col
                style={{
                  cursor: "pointer",
                  backgroundColor: p.color,
                  height: "160px",
                  overflow: "hidden",
                  alignItems: "center",
                  justifyContent: !p.logo ? "space-between" : "center",
                  borderRadius: "12px",
                  boxShadow: "2px 2px 10px #898989",
                  padding: p.logo ? Metrics.spacing.md : 0,
                }}
              >
                {p.logo ? (
                  <img
                    src={
                      p.logo?.base.length > 200
                        ? p.logo?.base
                        : `/api/uploads/${p.logo?.base}`
                    }
                    alt="project-logo"
                    style={{ width: "100%" }}
                  />
                ) : (
                  <Col
                    style={{
                      backgroundColor: "#e0e0e0",
                      justifyContent: "center",
                      alignItems: "center",
                      width: "100%",
                      height: !p.logo ? "80%" : "100%",
                    }}
                  >
                    <span
                      style={{ color: "#fff", fontSize: Metrics.fontSize.lg }}
                    >
                      logo
                    </span>
                  </Col>
                )}
                {p.showTitle && (
                  <Row>
                    <span
                      style={{
                        color: Colors.mainTheme.aux,
                        fontSize: Metrics.fontSize.sm,
                        fontFamily: "MADETommySoft-Bold",
                        textTransform: "uppercase",
                      }}
                    >
                      {p.title}
                    </span>
                  </Row>
                )}
              </Col>
            </Col>
          ))}
        </Carousel>
      </Col>
    </Col>
  );
};

export default Portfolio;
