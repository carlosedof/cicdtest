import React from "react";
import { Col } from "../../../../common";
import { Metrics } from "../../../../config";
import { BlockTitle } from "../../../../components";

const styles = {
  container: {
    marginTop: Metrics.spacing.lg,
    marginBottom: Metrics.spacing.xxxlg * 2,
    marginInline: Metrics.spacing.lg,
  },
  textCol: {
    paddingInline: Metrics.spacing.xxxlg,
  },
  text: {
    fontSize: Metrics.fontSize.sm,
    textAlign: "justify",
  },
};

const POC = () => (
  <Col name="poc" style={styles.container}>
    <BlockTitle title="Solicite sua prova de conceito" />
    <Col style={styles.textCol}>
      <h5 style={styles.text}>
        Sem compromisso, nós desenvolvemos uma &apos;amostra&apos; da sua idéia
        de sistema, para que você possa ver uma parte dele funcionando e decidir
        com mais tranquilidade por contratar nossos serviços.
      </h5>
      <h5 style={styles.text}>
        Entre em contato via{" "}
        <a
          target="_blank"
          href="https://api.whatsapp.com/send?phone=5548999999999"
          rel="noreferrer"
        >
          whatsapp
        </a>{" "}
        ou pelo formulário no final da página.
      </h5>
    </Col>
  </Col>
);

export default POC;
