import React, { useContext } from "react";
import { Button } from "antd";
import { AuthContext } from "../../../context/AuthContext";
import { Col } from "../../../common";

const Login = () => {
  const { handleLogin } = useContext(AuthContext);

  return (
    <Col>
      <Button onClick={handleLogin}>Login</Button>
    </Col>
  );
};

export default Login;
