import React from "react";
import { useParams } from "react-router";
import { Col, Row } from "../../../common";
import { Colors, Metrics } from "../../../config";
import { findProjectRequest } from "../../../services/data";
import Mask from "../../../assets/images/projects/mask.png";
import BackBtn from "../../../components/BackBtn";
import useFetch from "../../../hooks/useFetch";

const styles = {
  container: {
    padding: Metrics.spacing.lg,
  },
};

const ProjectPresentation = () => {
  const { id } = useParams();
  const [{ data: project }] = useFetch({
    provider: findProjectRequest,
    param: id,
    requestOnMount: true,
    initialData: [],
    resultHandler: {
      success: () => {},
    },
  });
  return (
    <Col style={styles.container}>
      <BackBtn />
      {project && (
        <Col>
          {project?.logo?.base && (
            <img
              src={
                project.logo?.base.length > 200
                  ? project.logo?.base
                  : `/api/uploads/${project.logo?.base}`
              }
              alt="logo"
              style={{
                width: "150px",
                backgroundColor: project?.color ? project?.color : "unset",
              }}
            />
          )}
          <Row style={{ alignItems: "center" }}>
            <span
              style={{
                fontSize: Metrics.fontSize.xsm,
                textTransform: "uppercase",
                fontWeight: "bolder",
              }}
            >
              Projeto:
            </span>
            <span
              style={{
                fontFamily: "MADETommySoft-Bold",
                fontSize: Metrics.fontSize.xlg,
                color: Colors.mainTheme.tertiary,
                marginBlock: Metrics.spacing.md,
                marginLeft: Metrics.spacing.sm,
              }}
            >
              {project?.title || "Não informado"}
            </span>
          </Row>
          <Row
            style={{ alignItems: "center", marginBottom: Metrics.spacing.lg }}
          >
            <span
              style={{
                fontSize: Metrics.fontSize.xsm,
                textTransform: "uppercase",
                fontWeight: "bolder",
              }}
            >
              {`Ano: ${project?.year || "Não informado"}`}
            </span>
          </Row>
          <Row
            style={{ alignItems: "center", marginBottom: Metrics.spacing.lg }}
          >
            <span
              style={{
                fontSize: Metrics.fontSize.xsm,
                textTransform: "uppercase",
                fontWeight: "bolder",
              }}
            >
              Descrição:
            </span>
            <span
              style={{
                fontFamily: "MADETommySoft",
                fontSize: Metrics.fontSize.sm,
                color: Colors.mainTheme.tertiary,
                marginLeft: Metrics.spacing.sm,
              }}
            >
              {project?.description || "não disponível"}
            </span>
          </Row>
          <Row
            style={{ alignItems: "center", marginBottom: Metrics.spacing.lg }}
          >
            <span
              style={{
                fontSize: Metrics.fontSize.xsm,
                textTransform: "uppercase",
                fontWeight: "bolder",
              }}
            >
              {`Publicado/Status: ${project?.status || "Não informado"}`}
            </span>
          </Row>
          <Row
            style={{ alignItems: "center", marginBottom: Metrics.spacing.lg }}
          >
            <span
              style={{
                fontSize: Metrics.fontSize.xsm,
                textTransform: "uppercase",
                fontWeight: "bolder",
              }}
            >
              {`Empresa: ${project?.company || "Não informado"}`}
            </span>
          </Row>
          <Row
            style={{ alignItems: "center", marginBottom: Metrics.spacing.lg }}
          >
            <span
              style={{
                fontSize: Metrics.fontSize.xsm,
                textTransform: "uppercase",
                fontWeight: "bolder",
              }}
            >
              Tecnologias utilizadas:
            </span>
          </Row>
          <Row
            style={{ alignItems: "center", marginBottom: Metrics.spacing.lg }}
          >
            <span
              style={{
                fontSize: Metrics.fontSize.xsm,
                textTransform: "uppercase",
                fontWeight: "bolder",
              }}
            >
              {`URL: ${project?.url || "Não informado"}`}
            </span>
          </Row>
          <Row style={{ flexWrap: "wrap", marginBlock: Metrics.spacing.md }}>
            {project?.images?.map((im) => (
              <Col
                style={{
                  position: "relative",
                  marginInline: Metrics.spacing.md,
                  width: "175px",
                  height: "350px",
                  overflow: "hidden",
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <img
                  src={Mask}
                  alt="iphone-mask"
                  style={{ width: "175px", position: "absolute" }}
                />
                <img
                  src={
                    im.base?.length > 200 ? im.base : `/api/uploads/${im.base}`
                  }
                  alt="proj-img"
                  style={{ width: "160px", borderRadius: "18px" }}
                />
              </Col>
            ))}
          </Row>
          <Row>
            {project?.projectLanguages?.map((pl) => (
              <img
                src={pl.language.icon}
                alt="icon"
                style={{ width: "30px" }}
              />
            ))}
          </Row>
        </Col>
      )}
    </Col>
  );
};

export default ProjectPresentation;
