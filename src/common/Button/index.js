import React from "react";
import { Button as Btn } from "antd";
import { Colors, Metrics } from "../../config";

const styles = {
  button: {
    backgroundColor: Colors.mainTheme.secondary,
    color: Colors.mainTheme.white,
    textTransform: "uppercase",
    borderRadius: "6px",
    border: "none",
    fontSize: Metrics.fontSize.xsm,
    fontFamily: "Nunito",
    fontWeight: "700",
  },
};

const Button = ({ children, min, tertiary, style, ...rest }) => (
  <Btn
    {...rest}
    style={{ ...styles.button, width: min ? "unset" : "100%", ...style }}
  >
    {children}
  </Btn>
);

export default Button;
