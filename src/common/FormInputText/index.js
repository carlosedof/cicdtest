/* eslint-disable jsx-a11y/label-has-associated-control */
import { useFormikContext } from "formik";
import { FormItem, Input } from "formik-antd";
import React from "react";

const FormInputText = ({
  label,
  onChange: onChangeParam,
  placeholder,
  fullWidth,
  showValidateSuccess,
  forcePlaceholder,
  name,
  required,
  disabled,
  ...rest
}) => {
  const { getFieldMeta } = useFormikContext();
  const { error, touched } = getFieldMeta(name);

  const onChange = (newValue) => {
    if (!disabled) {
      if (onChangeParam) {
        onChangeParam(newValue);
      }
    }
  };

  return (
    <FormItem
      style={fullWidth ? { width: "100%" } : {}}
      name={name}
      required={required}
      showValidateSuccess={showValidateSuccess}
    >
      {
        // FIXME: Verificar para tilizar Form.Item do Antd
      }
      <label style={{ color: error && touched && "#ff4d4f" }}>
        {label && (required ? `${label} *` : label)}
      </label>
      <Input
        mode="outlined"
        name={name}
        required={required}
        disabled={disabled}
        onChange={(e) => onChange(e.target.value)}
        placeholder={forcePlaceholder || !label ? placeholder : ""}
        {...rest}
      />
    </FormItem>
  );
};

export default FormInputText;
