import React from "react";
import { Checkbox as ACheckbox } from "antd";
import { Metrics } from "../../config";

const Checkbox = (props) => (
  <ACheckbox {...props} style={{ marginBottom: Metrics.spacingSM }} />
);

export default Checkbox;
