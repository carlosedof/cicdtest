import React, { useMemo } from "react";
import "./styles.css";

export default ({
  onClick,
  children,
  responsive,
  reverse,
  fullWidthResponsive,
  className = " ",
  style,
}) => {
  const getClasses = useMemo(() => {
    let classname = `${className} `;
    if (responsive && !fullWidthResponsive) {
      classname = classname.concat("row-responsive");
    }
    if (reverse) {
      classname = classname.concat(" row-responsive");
    } else {
      classname = classname.concat(" row-common");
    }
    if (responsive && fullWidthResponsive) {
      classname = classname.concat(" row-responsive-full");
    }
    if (!responsive && fullWidthResponsive) {
      classname = classname.concat(" row-full");
    }
    if (onClick) {
      classname = classname.concat(" hoverable");
    }
    return classname;
  }, [reverse, responsive, fullWidthResponsive]);
  return (
    <div
      role="button"
      tabIndex={0}
      onClick={onClick}
      className={getClasses}
      style={{ ...style }}
    >
      {children}
    </div>
  );
};
