import React from "react";
import { useFormikContext } from "formik";
import Checkbox from "../Checkbox";

export default ({
  disabled,
  value,
  onChange: onChangeParam,
  name,
  label,
  required,
  ...rest
}) => {
  const { setFieldValue } = useFormikContext();

  const onChange = (newValue) => {
    if (!disabled) {
      if (onChangeParam) {
        onChangeParam(newValue);
      } else {
        setFieldValue(name, newValue);
      }
    }
  };

  return (
    <Checkbox
      {...rest}
      checked={value}
      onChange={(e) => onChange(e.target.checked)}
    >
      {required ? `${label} *` : label}
    </Checkbox>
  );
};
