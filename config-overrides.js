const { override, fixBabelImports, addLessLoader } = require('customize-cra');
const rewireReactHotLoader = require('react-app-rewire-hot-loader');
const path = require('path');
const fs = require('fs');

const lessToJs = require('less-vars-to-js');

const themeVariables = lessToJs(
    fs.readFileSync(path.join(__dirname, './src/assets/less/theme.less'), 'utf8')
);

module.exports = override(
    config => {
        if (config.mode === 'development') {
            config.resolve.alias['react-dom'] = '@hot-loader/react-dom';
        }
        config = rewireReactHotLoader(config, config.mode);
        return config;
    },
    fixBabelImports('import', {
        libraryName: 'antd',
        libraryDirectory: 'es',
        style: true,
    }),
    fixBabelImports('formik-antd', {
        libraryName: 'formik-antd',
        libraryDirectory: 'es',
        style: false,
    }),
    addLessLoader({
        lessOptions: {
            javascriptEnabled: true,
            modifyVars: themeVariables,
        },
    })
);
